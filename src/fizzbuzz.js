const fizzbuzz = (num) => {
     if ( num % 7 == 0 && num % 5 == 0 ){
        return "fizzbuzz";
    }
    else if ( num % 7 == 0 ){
         return "fizz";
        }
    else if ( num % 5 == 0){ 
        return "buzz";
    }
    else if (!num) {
        return  "Error!";
    }
     else{
        return "";
    }
}

exports.fizzbuzz = fizzbuzz;