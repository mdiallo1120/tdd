const expect = require('chai').expect;
const {calc} = require('../src/calc');

describe('calc', () => {
	describe('Addition', () => {
		it('1 + 1 should be equals to 2', () => {
			expect(calc(1, 1,'+')).to.equal(2)
		})
		
	})

	describe('Subtraction', () => {
		it('1 - 1 should be equals to 0', () => {
			expect(calc(1, 1,'-')).to.equal(0)
		})
		
	})

	describe('Multiplication', () => {
		it('1 * 1 should be equals to 1', () => {
			expect(calc(1, 1,'*')).to.equal(1)
		})
		
	})

	describe('Division', () => {
		it('1 / 1 should be equals to 1', () => {
			expect(calc(1, 1,'/')).to.equal(1)
		})
	})
})


//it('1 + 1 should be equals to 2', () => {
    // expect(calc.add(1, 1)).to.equal(2).

